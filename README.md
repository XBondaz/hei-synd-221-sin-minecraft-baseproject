
# SIN Minecraft ElectricalAge

## Introduction

In this project we will communicate between a Minecraft world and a program. This program will allow us to control the different production units and to put the values on a web page. And to store these values on influxDB which we can access with the Grafana tool.


## Function

## Core
Core

The core is the central component of our architecture. It allows to create DataPoints. There are two types of Datapoints, either float or boolean. Once created they are stored on a Hashmap.

## Modbus
Our program will connect to a Modbus TCP server in the Minecraft game. This will allow us to access information that we will need like (GRID_U_FLOAT etc...). To do this we will use the Modbus4j library. The Modbus registers we will use are stored in a csv file. We will read it and create the necessary variables. The data will then be processed, scaled and stored in a DataPoint.

## Field
The Field has the task to link the Modbus with the DataPoint, as well as to update the Modbus when the read() and write() methods are used on objects of type BooleanDataPoint and FloatDataPoint.

## Database
Allows you to keep track of the history of the field. Each value will be sent to influxDB. InfluxDB allows to store data. Thanks to grafana we can see in graph form the data according to time.

## Web
This component contains a WebSocket that allows us to connect to a dedicated web page. We can read from it real time values and manipulate variables such as the power of the coal plant.

## SmartConroller
Allows the management and regulation of production units in the Minecraft world.

## Installation
You can download our complet programm with this link:

[Completprogramm](https://gitlab.com/XBondaz/hei-synd-221-sin-minecraft-baseproject/-/blob/master/hei-synd-221-sin-minecraft-baseproject__final_.zip)

And the csv file with this link:

[csvFile](https://gitlab.com/XBondaz/hei-synd-221-sin-minecraft-baseproject/-/blob/master/ModbusMap.csv)


## .jar
.jar

This project contains a .jar file. It uses the server influx.hevs.ch to connect to the Minecraft world on your PC. 
Parameter: <db_url> <db_org> <db_bucket> <db_tocken> <modbus_host> <modbus_port>
```
Java -jar Minecraft.jar https://influx.sdi.hevs.ch Sin04 Sin04 AioSejq1-XFqk3ZwdMbW9TF68cBG9jnOPPg5BV2BiPUOsXKQUhtH2pUMg6il0rnD21nLV6QuUg4SaIumWROcJg== localhost 1502
```
[Jarlink](https://gitlab.com/XBondaz/hei-synd-221-sin-minecraft-baseproject/-/blob/master/Minecraft.jar)
## javadoc

The javadoc is directly downloadable in this project [link](https://gitlab.com/XBondaz/hei-synd-221-sin-minecraft-baseproject/-/tree/master/javadoc/hei).

## Authors

- [@marieburcher000](https://www.gitlab.com/marieburcher000)
- [@XBondaz](https://www.gitlab.com/XBondaz)

