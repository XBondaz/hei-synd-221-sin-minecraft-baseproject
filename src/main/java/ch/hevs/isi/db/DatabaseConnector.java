package ch.hevs.isi.db;


import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class DatabaseConnector implements DataPointListener {

    private static DatabaseConnector dbc = null;

    private URL url;
    private String token;
    public static TimeManager tm;
    public static long timestamp = 0;


    /**
     * Constructor of new DatabaseConnector
     */
    private DatabaseConnector() {
        tm = new TimeManager(3);
    };


    /**
     * Use to configure the URL. For connection to influx.hevs.ch
     * @param baseURL address https
     * @param token key for connection
     * @param bucket the name of the bucket to work with
     * @param organization the name of your organisation
     */
    public void configure(String baseURL, String token, String bucket, String organization){
        this.token = token;

        try {
            url = new URL(baseURL + "/api/v2/write?org=" + organization + "&bucket=" + bucket );
            System.out.println(url);
        }catch (IOException e){
            throw new RuntimeException();
        }

    }

    /**
     * If none has been created, the function create a new instance.
     * If one exist the function returned it.
     * @return Return the instance of DatabaseConnector
     */

    public static DatabaseConnector getInstance(){
        if(dbc == null){
            dbc = new DatabaseConnector();
        }
        return dbc;
    }



    /** We open a url connection. We define our structure for influx (measurement, label etc..)
     *
     * @param label this is label in field (ex. GRID_U_FLOAT)
     * @param value this is value for teh label above
     */
    private void pushToDB(String label, String value)  {
      try{
          HttpURLConnection connection =  (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Authorization", "Token "+ token);
        connection.setRequestProperty("Content-Type", "Texte/plain; charset=utf-8");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
        String out = "Minecraft " + label + "=" + value + " " + timestamp ;
        writer.write(out);
        writer.flush();
        int responseCode = connection.getResponseCode();
        System.out.println(responseCode);
      }
      catch (MalformedURLException e){
          e.printStackTrace();
      }
      catch (IOException e){
          e.printStackTrace();
      }
    }





    /**
     * This method while transfer the boolean value to an influxDb database
     * @param booleanDataPoint The new Value
     */
    public void onNewValue(BooleanDataPoint booleanDataPoint) {

            pushToDB(booleanDataPoint.getLabel(), String.valueOf(booleanDataPoint.getValue()));
    }


    /**
     * This method while transfer the Float value to an influxDb database. And we control the time,
     * between influx and Minecraft.
     * @param floatDataPoint The new Value
     */
    public void onNewValue(FloatDataPoint floatDataPoint) {
        if(floatDataPoint.getLabel().equals("CLOCK_FLOAT")){
            tm.setTimestamp(floatDataPoint.getValue());
            timestamp = tm.getNanosForDB();
        }
        if(timestamp != 0){
            pushToDB(floatDataPoint.getLabel(), String.valueOf(floatDataPoint.getValue()));
        }


    }

    public static void main(String[] args) {
    DatabaseConnector dc = new DatabaseConnector();
    dc.configure("https://influx.sdi.hevs.ch/","AioSejq1-XFqk3ZwdMbW9TF68cBG9jnOPPg5BV2BiPUOsXKQUhtH2pUMg6il0rnD21nLV6QuUg4SaIumWROcJg==","SIn04","SIn04");
    dc.onNewValue(new FloatDataPoint("Test2",true));


    }
}
