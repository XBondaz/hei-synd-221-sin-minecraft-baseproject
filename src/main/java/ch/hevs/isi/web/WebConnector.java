package ch.hevs.isi.web;

import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import java.net.InetSocketAddress;

public class WebConnector implements DataPointListener {
    private static WebConnector wc = null;
    private WebSocketServer wss = null;

    /**
     * Constructor of WebConnector
     */
    private WebConnector() {
        wss = new WebSocketServer(new InetSocketAddress(8888)) {

            /**
             * Send a welcome message when a webSocket is connected
             */

            @Override
            public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
                webSocket.send("Welcome");
            }


            /**
             * Close a webSocket when one connection disconnect
             */

            @Override
            public void onClose(WebSocket webSocket, int i, String s, boolean b) {

            }

            /**
             * Used to read the new Value send by the socket and then set them in the correct DataPoint
             */

            @Override
            public void onMessage(WebSocket webSocket, String s) {
                // Message s: "<label>=<value>;

                String d[] = s.split("=");
                String value = d[1];
                DataPoint dp = DataPoint.getDataPointFromLabel(d[0]);

                if (value.equalsIgnoreCase("true"))
                    dp.setValue(true);
                else if (value.equalsIgnoreCase("false"))
                    dp.setValue(false);
                else
                    dp.setValue(Float.parseFloat(value));
            }
            /**
             * Unimplemented
             */

            @Override
            public void onError(WebSocket webSocket, Exception e) {

            }
            /**
             * Unimplemented
             */

            @Override
            public void onStart() {

            }
        };

        wss.start();

    }
    /**
     * If none has been created, the function create a new instance.
     * If one exist the function returned it.
     * @return Return the instance of WebConnector
     */

    public static WebConnector getInstance(){
        if(wc == null){
            wc = new WebConnector();
        }
        return wc;
    }

    /**
     * Use to push on the web page
     * @param label name of the value
     * @param value value
     */
    private void pushToWebPages(String label, String value){
        for (WebSocket ws: wss.getConnections()) {
            ws.send(label +"=" + value);
        }
    }
    /**
     * This method while transfer the Float value to the Web
     * @param booleanDataPoint The new Value
     */

    public void onNewValue(BooleanDataPoint booleanDataPoint) {

        pushToWebPages(booleanDataPoint.getLabel(), String.valueOf(booleanDataPoint.getValue()));
    }
    /**
     * This method while transfer the Float value to the Web
     * @param floatDataPoint The new Value
     */

    public void onNewValue(FloatDataPoint floatDataPoint) {

        pushToWebPages(floatDataPoint.getLabel(), String.valueOf(floatDataPoint.getValue()));
    }

    public static void main(String[] args) {
         WebConnector wb = WebConnector.getInstance();

    }
}
