package ch.hevs.isi.field;

import ch.hevs.isi.core.DataPoint;


import java.util.HashMap;
import java.util.Map;


public abstract class ModbusRegister {
    protected int address;
    protected static Map<DataPoint, ModbusRegister> map = new HashMap<>();
    protected String label;

    /**
     * Use to get a Register from a DataPoint
     * @param dp The DataPoint
     * @return The corresponding Register
     */

    public static ModbusRegister getRegisterFromDataPoint(DataPoint dp) {

        return map.get(dp);
    }
public abstract void read();

public abstract void write();

    /**
     * Use to read all value in the HasMap with the poll methode
     */
    public static void poll(){
    for(ModbusRegister mr:map.values()) {
        mr.read();
    }


}
    public static void main(String[] args) {

        ModbusAccessor ma = ModbusAccessor.getInstance();
        ma.connect("localhost",1502, 1);



    }

}


