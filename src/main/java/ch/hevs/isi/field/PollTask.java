package ch.hevs.isi.field;

import java.util.TimerTask;

public class PollTask extends TimerTask {

    /**
     * The task that will poll all register
     */

    public void run(){

        ModbusRegister.poll();
    }
}
