package ch.hevs.isi.field;


import ch.hevs.isi.core.FloatDataPoint;

public class FloatRegister extends ModbusRegister{

    int range;
    int offset;

private FloatDataPoint floatDataPoint;
private static ModbusAccessor ma = ModbusAccessor.getInstance();


    /**
     * Construcptr of FloatDataPoint class
     * @param label the label of the data
     * @param isOutput if it is an output= true
     * @param address the address of the data
     * @param range value for calculation
     * @param offset value for calculation
     */
    public FloatRegister(String label, boolean isOutput, int address, int range, int offset) {
        floatDataPoint = new FloatDataPoint(label, isOutput);
        this.address = address;
        map.put(floatDataPoint, this);
        this.range = range;
        this.offset = offset;
        this.label = label;
    }

    /**
     * Used to read the value of the actual Datapoint with the register Object
     */
    public void read(){
    float val = ma.readFloat(address);
    floatDataPoint.setValue(val * range + offset);
}

    /**
     * Used to write the value of the actual Datapoint with the register Object
     */
    public void write(){

        ma.writeFloat(address, floatDataPoint.getValue() / range - offset);
    }

}
