package ch.hevs.isi.field;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.locator.BaseLocator;

public class ModbusAccessor {
   public ModbusFactory modbusFactory = null;
   public ModbusMaster tcpMaster = null;
   private static ModbusAccessor ma = null;
   private byte unit;

    /**
     * Constructorod ModbusAcessor class
     */
    private ModbusAccessor() {
        ma = this;
    };

    /**
     * If none has been created, the function create a new instance.
     * If one exist the function returned it.
     * @return Return the instance of ModbussAccessor
     */

    public static ModbusAccessor getInstance(){
        if (ma == null){
            return ma = new ModbusAccessor();}

            return ma;
    }

    /**
     * Use this method to connect the ModbussAccessor to the modbus RTU
     * @param ipAddress The ip Address of the modbus
     * @param port The port
     * @param unit format
     */

    public void connect(String ipAddress, int port, int unit) {
        modbusFactory = new ModbusFactory();
        IpParameters ip = new IpParameters();
        ip.setHost(ipAddress);
        ip.setPort(port);
        this.unit =(byte) unit;
        tcpMaster = modbusFactory.createTcpMaster(ip, true);
        while (true) {
            try {
                tcpMaster.init();
                break;
            } catch (ModbusInitException e) {
                System.out.println("Unable to connect to the server");
            }
            try {
                Thread.sleep(1000);

            } catch (InterruptedException e) {
            }
            //continue;
        }
    }

    /**
     * Function use to read Float value from the modbus
     * The returned value is between 0 and 1
     * @param regAddress The register address of the float that we will read
     * @return  Return -1 if an error occurred
     */

    public float readFloat(int regAddress){
        try{
            return tcpMaster.getValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT)).floatValue();

        } catch (ModbusTransportException e) {
            System.out.println("Unable to connect to the modbus");
        } catch (ErrorResponseException e) {
            System.out.println("Modbus error");
        }

        return -1;
    }

    /**
     * Function use to write Float value to the modbus
     * @param regAddress The register address of the float that we want to write
     * @param newValue The value that we want to write
     * @return Return -1 if an error occurred and 0 if no error occurred
     */

    public float writeFloat(int regAddress, float newValue){
        try{
             tcpMaster.setValue(BaseLocator.holdingRegister(1, regAddress, DataType.FOUR_BYTE_FLOAT), newValue);
            return 0;
        }
        catch(ModbusTransportException e){
            System.out.println("Unable to connect to the modbus RTU");

        }
        catch(ErrorResponseException e){
            System.out.println("Modbus error");
        }
        return -1;
    }

    /**
     * Function use to write a Boolean value to the modbus
     * @param register  The register address of the Boolean that we want to write
     * @param value Return the value of the register between 0 and 1 or -1 if an error occured
     * @return  Return -1 if an error occurred and 0 if no error occurred
     */

    public float writeBoolean(int register, boolean value){
    try {
        tcpMaster.setValue(BaseLocator.coilStatus(1, register), value);
        return 0;
    }catch (ModbusTransportException e){
        System.out.println("Unable to connect to the Modbus RTU");
    }catch (ErrorResponseException e){
        System.out.println("Modbus response Error");
    }
    return -1;
    }

    /**
     * Function use to read Boolean value from the modbus RTU
     * @param register The register address of the Boolean that we want to read
     * @return Return the value of the register or -1 if an error occurred
     */

    public float readBoolean(int register){
        try{
            boolean output = tcpMaster.getValue(BaseLocator.coilStatus(unit,register)).booleanValue();
            if(output == true) {

                return 1;
            } else {
                return 0;
                }

        } catch (ModbusTransportException e) {
            System.out.println("Unable to connect to the modbus");
        } catch (ErrorResponseException e) {
            System.out.println("Modbus error");
        }

        return -1;

    }

    public static void main(String[] args) {

        ModbusAccessor ma = new ModbusAccessor();
        ma.connect("localhost",1502 , 1);
        System.out.println("Grid U = " + ma.readFloat(89));
        System.out.println("Grid U = " + ma.readBoolean(89));

        System.out.println("Solar = " + ma.writeBoolean(401,false));
        System.out.println("Wind = " + ma.writeBoolean(405,false));








    }








}
