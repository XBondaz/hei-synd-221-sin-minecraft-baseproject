package ch.hevs.isi.field;


import ch.hevs.isi.core.BooleanDataPoint;
import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.DataPointListener;
import ch.hevs.isi.core.FloatDataPoint;

import java.util.Timer;

public class FieldConnector implements DataPointListener {

    private static FieldConnector fc = null;

    /**
     * Constructor of FieldConnector class
     */
    private FieldConnector() {

    }
    /**
     * If none has been created, the function create a new instance.
     * If one exist the function returned it.
     * @return Return the instance of FieldConnector
     */


    public static FieldConnector getInstance() {

        if (fc == null) {
            fc = new FieldConnector();
        }
        return fc;
    }

    /**
     * Push the value in the field
     * @param DP a dataPoint
     */

    private void pushToField(DataPoint DP) {

        ModbusRegister mr = ModbusRegister.getRegisterFromDataPoint(DP);
        if ( mr != null && DP.isOutput()) {
            mr.write();
        }
    }

    /**
     * method while push the boolean value to the field
     * @param booleanDataPoint The new Value
     */

    public void onNewValue(BooleanDataPoint booleanDataPoint) {

        pushToField(booleanDataPoint);
    }

    /**
     * method while push the Float value to the field
     * @param floatDataPoint The new Value
     */

    public void onNewValue(FloatDataPoint floatDataPoint) {

        pushToField(floatDataPoint);
    }



   /* public void onNewValue(DataPoint dp) {





    }
*/

    /**
     * Use to created a time with our PollTask
     * @param period time
     */
    public void startPolling(int period) {

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new PollTask(), 0, period);
    }
}
