package ch.hevs.isi.field;

import ch.hevs.isi.utils.Utility;

import java.io.BufferedReader;
import java.io.IOException;

public class FieldConfig {

    /**
     * Use to decode the csv file.
     * @param fileName the name of the file
     */
    public static void getFile(String fileName){

            BufferedReader br = Utility.fileParser(null, fileName);
            if (br != null) {
                try {
                    String line = br.readLine();

                    while (line != null) {

                        if (!line.startsWith("Label")) {
                            String config[] = line.split(";");

                            String label = config[0];
                            boolean isFloat = config[1].equals("F");
                            boolean isOutput = config[3].equals("Y");
                            int address = Integer.parseInt(config[4]);
                            int range= Integer.parseInt(config[5]);
                            int offset= Integer.parseInt(config[6]);
                            if (!label.equalsIgnoreCase("label")) {
                                if (isFloat) {
                                    new FloatRegister(label, isOutput, address, range, offset);
                                } else {
                                    new BooleanRegister(label, address, isOutput);
                                }
                            }
                        }

                        line = br.readLine();
                    }

                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }














