package ch.hevs.isi.field;

import ch.hevs.isi.core.BooleanDataPoint;

public class BooleanRegister extends ModbusRegister{

private BooleanDataPoint booleanDataPoint;
private static ModbusAccessor ma = ModbusAccessor.getInstance();
    /**
     * Constructor of BooleanRegister
     * @param label the label
     * @param address the address of the label
     * @param isOutput if it is an output = true
     */
    public BooleanRegister(String label, int address, boolean isOutput) {
    booleanDataPoint = new BooleanDataPoint(label, isOutput);
    this.address = address;
    this.label = label;
    map.put(booleanDataPoint, this);
}

    /**
     * Used to read the value of the actual Datapoint with the register Object
     */
    public void read(){
    float val = ma.readBoolean(address);
    booleanDataPoint.setValue(val);
}
    /**
     * Used to write the value of the actual Datapoint with the register Object
     */
    public void write(){
    ma.writeBoolean(address, booleanDataPoint.getValue());
    }


}
