package ch.hevs.isi.SmartControl;

import ch.hevs.isi.core.DataPoint;
import ch.hevs.isi.core.FloatDataPoint;
import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConfig;
import ch.hevs.isi.field.ModbusAccessor;

import java.util.TimerTask;

public class SmartControl extends TimerTask {
    FloatDataPoint BATT_CHRG_FLOAT;
    FloatDataPoint SOLAR_P_FLOAT;
    FloatDataPoint COAL_P_FLOAT;
    FloatDataPoint HOME_P_FLOAT;
    FloatDataPoint PUBLIC_P_FLOAT;
    FloatDataPoint BUNKER_P_FLOAT;
    FloatDataPoint WIND_FLOAT;
    FloatDataPoint REMOTE_COAL_SP;
    FloatDataPoint REMOTE_FACTORY_SP;
    FloatDataPoint COAL_AMOUNT;

    private int mode = 0;



    /**
     * Constructor of SmartControl class
     */
    public SmartControl() {


       BATT_CHRG_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BATT_CHRG_FLOAT");
       SOLAR_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("SOLAR_P_FLOAT");
       COAL_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_P_FLOAT");
       HOME_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("HOME_P_FLOAT");
       PUBLIC_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("PUBLIC_P_FLOAT");
       BUNKER_P_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("BUNKER_P_FLOAT");
       WIND_FLOAT = (FloatDataPoint) DataPoint.getDataPointFromLabel("WIND_FLOAT");
       REMOTE_COAL_SP = (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_COAL_SP");
       REMOTE_FACTORY_SP= (FloatDataPoint) DataPoint.getDataPointFromLabel("REMOTE_FACTORY_SP");
        COAL_AMOUNT = (FloatDataPoint) DataPoint.getDataPointFromLabel("COAL_AMOUNT");
    }

    /**
     * Use to regulate our Minecraft world
     */
    public void controlloop(){
        float productionWatt = WIND_FLOAT.getValue() + SOLAR_P_FLOAT.getValue() + COAL_P_FLOAT.getValue();
       float consumptionWatt = BUNKER_P_FLOAT.getValue() + HOME_P_FLOAT.getValue() + PUBLIC_P_FLOAT.getValue();

        switch (mode){
            case 0:
                REMOTE_COAL_SP.setValue(0);
                if (BATT_CHRG_FLOAT.getValue() <= 0.55f){
                    mode = 1;
                    controlloop();
                }else if (BATT_CHRG_FLOAT.getValue() >= 0.85f){
                    mode = 2;
                    controlloop();
                }
                else
                { if (consumptionWatt < productionWatt) {
                    REMOTE_FACTORY_SP.setValue(0.20f);
                } else{
                REMOTE_FACTORY_SP.setValue(0);}
                }
                break;
            case 1 :

                if (BATT_CHRG_FLOAT.getValue() <= 0.55f){
                    REMOTE_FACTORY_SP.setValue(0);
                    if (productionWatt <= consumptionWatt && COAL_AMOUNT.getValue()>= 0.075f){
                        REMOTE_COAL_SP.setValue(1);
                    }
                }
                else {
                    mode = 0;
                    controlloop();
                }
                break;

            case 2 :

                if(BATT_CHRG_FLOAT.getValue() >= 0.85f){
                    REMOTE_COAL_SP.setValue(0);
                    REMOTE_FACTORY_SP.setValue(1f);
                } else {
                    mode = 0;
                    controlloop();
                }
                break;
        }
    }

    /**
     * The task that will poll the controlloop
     */
    @Override
    public void run() {

        controlloop();
    }

    public static void main(String[] args) throws InterruptedException {
        FieldConfig.getFile("ModbusMap.csv");
        DatabaseConnector dc = DatabaseConnector.getInstance();
        dc.configure("https://influx.sdi.hevs.ch/","AioSejq1-XFqk3ZwdMbW9TF68cBG9jnOPPg5BV2BiPUOsXKQUhtH2pUMg6il0rnD21nLV6QuUg4SaIumWROcJg==","SIn04","SIn04");
        ModbusAccessor ma = ModbusAccessor.getInstance();
        ma.connect("localhost", 1502, 1);
        SmartControl smartControl = new SmartControl();
        while(true) {
            smartControl.controlloop();
            Thread.sleep(1000);
        }
    }
}

