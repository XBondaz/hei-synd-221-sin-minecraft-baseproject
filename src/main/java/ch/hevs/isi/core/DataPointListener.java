package ch.hevs.isi.core;

public interface DataPointListener  {
    /**
     * Used in connector to implement onNewValue method
     * @param booleanDataPoint The new Value
     */
    void onNewValue(BooleanDataPoint booleanDataPoint);
    /**
     * Used in connector to implement onNewValue method
     * @param floatDataPoint The new Value
     */
    void onNewValue(FloatDataPoint floatDataPoint);

}
