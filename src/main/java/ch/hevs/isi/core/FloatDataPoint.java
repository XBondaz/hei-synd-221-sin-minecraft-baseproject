package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

public class FloatDataPoint extends DataPoint {
    private float value;

    /**
     * Constructor of new BooleanDataPoint
     * @param label The label of the data
     * @param isOutput If it's an output = true
     */
    public FloatDataPoint(String label, Boolean isOutput) {

        super(label, isOutput);
    }

    @Override
    public void setValue(boolean value) {

    }
    /**
     * Use to set a new Value. When a new value is written
     * We update it by calling the appropriate way for each connector class
     * @param value The new float value to set
     * */
    public void setValue(float value) {

        this.value = value;
        FieldConnector fc = FieldConnector.getInstance();
        fc.onNewValue(this);
        DatabaseConnector db = DatabaseConnector.getInstance();
        db.onNewValue(this);
        WebConnector wc = WebConnector.getInstance();
        wc.onNewValue(this);
    }
    /**
     * Use to get the value of the datapoint
     * @return return the value in the datapoint
     */
    public float getValue() {

        return this.value;
    }
}
