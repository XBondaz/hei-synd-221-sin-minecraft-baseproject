package ch.hevs.isi.core;

import java.util.HashMap;
import java.util.Map;

public abstract class DataPoint {

    private String label;
    private boolean isOutput;
    private static Map<String, DataPoint> mapDataPoint = new HashMap<String, DataPoint>();

    /**
     * The constructor for a DataPoint. It's protected while we don't want datapoint.
     * We juste want extensive classes of DataPoint for example a BooleanDataPoint
     * @param label The label of the data
     * @param isOutput If it is an output = true
     */
    protected DataPoint(String label, Boolean isOutput) {

        this.label = label;
        this.isOutput = isOutput;
        mapDataPoint.put(label, this);
    }

    /**
     * Method used to find a DataPoint with a label
     * @param label The label of the data
     * @return return the DataPoint that correspond to the label
     */
    public static DataPoint getDataPointFromLabel(String label) {
        return mapDataPoint.get(label);
    }

    /**
     * Used to get the label
     * @return return the label of the DataPoint
     */

    public String getLabel() {
        return this.label;
    }

    /**
     * Use to get if it's an output or not
     * @return True it's an output, false not
     */

    public boolean isOutput() {
        return this.isOutput;
    }


    /**
     * Use to created a function in extends class
     * @param value
     */
    public abstract void setValue(boolean value);
    /**
     * Use to created a function in extends class
     * @param value
     */
    public abstract void setValue(float value);

    public static void main(String[] args) {
      FloatDataPoint fd = new FloatDataPoint("float", false);
      fd.setValue(52f);
      System.out.println(fd.getLabel() + " " + fd.getValue() + "is output : " + fd.isOutput());



    }
}
