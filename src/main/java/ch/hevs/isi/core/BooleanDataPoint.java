package ch.hevs.isi.core;

import ch.hevs.isi.db.DatabaseConnector;
import ch.hevs.isi.field.FieldConnector;
import ch.hevs.isi.web.WebConnector;

public class BooleanDataPoint extends DataPoint {
    private boolean value;

    /**
     * Constructor of new BooleanDataPoint
     * @param label The label of the data
     * @param isOutput If it is an output = true
     */
    public BooleanDataPoint(String label, Boolean isOutput) {

        super(label, isOutput);
    }

    /**
     * Use to set a new Value. When a new value is written, we update it by calling the appropriate way for each
     * Connector Classes.
     * @param value The new boolean value to set
     */
    public void setValue(boolean value) {
        this.value = value;

        FieldConnector fc = FieldConnector.getInstance();
        fc.onNewValue(this);
        DatabaseConnector db = DatabaseConnector.getInstance();
        db.onNewValue(this);
        WebConnector wc = WebConnector.getInstance();
        wc.onNewValue(this);
    }

    /**
     * Use to get the value of the datapoint
     * @return return the value in the datapoint
     */
    public boolean getValue() {
        return this.value;
    }

    @Override
    public void setValue(float value) {
        this.value = (value == 1 ? true:false);
        FieldConnector FC = FieldConnector.getInstance();
        FC.onNewValue(this);
        DatabaseConnector DB = DatabaseConnector.getInstance();
        DB.onNewValue(this);
        WebConnector WB = WebConnector.getInstance();
        WB.onNewValue(this);
    }
}
